/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author ACER
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
        // getByTel
        System.out.println(cs.getByTel("0801234567"));
        
        // add new customer
        Customer cus1 = new Customer("beem", "0909329910");
        cs.addNew(cus1);
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
        
        // update , delete customer
        Customer delCus = cs.getByTel("0909329910");
        delCus.setTel("0909329911");
        cs.update(delCus);
        System.out.println("After Updated");
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
        
        cs.delete(delCus);
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
    }
}
