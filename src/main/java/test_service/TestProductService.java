/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Product;
import com.werapan.databaseproject.service.ProductService;

/**
 *
 * @author ACER
 */
public class TestProductService {
    public static void main(String[] args) {
        ProductService pd = new ProductService();
        // getAll
        System.out.println("getAll");
        for(Product product : pd.getProducts()){
            System.out.println(product);
        }
        // getById
        System.out.println("getById");
        System.out.println(pd.getById(1));
        
        // add new
        Product r1 = new Product("magaron", 50, "-", "-", "-", 2);
        System.out.println("add new");
        pd.addNew(r1);
        for(Product product : pd.getProducts()){
            System.out.println(product);
        }
        
        // update 
        Product upPro = pd.getById(6);
        upPro.setPrice(60);
        pd.update(upPro);
        System.out.println("After Updated");
        for(Product product : pd.getProducts()){
            System.out.println(product);
        } 
        
        // delete
        Product delPro = pd.getById(6);
        System.out.println("delete");
        delPro.setId(6);
        pd.delete(delPro);
        for(Product product : pd.getProducts()){
            System.out.println(product);
        }
    }
}
